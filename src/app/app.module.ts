import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavLeftComponent } from './nav-left/nav-left.component';
import { SettingsMenuComponent } from './settings-menu/settings-menu.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import { MainContentComponent } from './main-content/main-content.component';
import { RightSidebarComponent } from './right-sidebar/right-sidebar.component';


@NgModule({
  declarations: [
    AppComponent,
    NavLeftComponent,
    SettingsMenuComponent,
    LeftSidebarComponent,
    MainContentComponent,
    RightSidebarComponent,
  
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
